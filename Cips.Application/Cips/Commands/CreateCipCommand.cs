using System;
using Cips.Application.Cips.Events;
using Cips.Application.Common.Interfaces;
using Cips.Domain.Entities;

namespace Cips.Application.Cips.Commands
{
    public class CreateCipRequest
    {
        public decimal Total  { get; set; }
        public string CurrencyCode { get; set; } 
        public string UserEmail { get; set; }
    }
    
    public class CreateCipCommand
    {
        private IEventBus _eventBus;

        public CreateCipCommand(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public Cip Handle(CreateCipRequest request) 
        {
            // Do nothing
            var cip = new Cip()
            {
                CipCode = Guid.NewGuid(),
                Total = request.Total,
                CurrencyCode = request.CurrencyCode,
                UserEmail = request.UserEmail
            };

            _eventBus.Publish(new CipCreatedEvent(cip));
            return cip;
        }
 
    }
}